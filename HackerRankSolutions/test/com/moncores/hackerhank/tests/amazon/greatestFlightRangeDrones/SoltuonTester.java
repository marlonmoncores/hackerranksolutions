package com.moncores.hackerhank.tests.amazon.greatestFlightRangeDrones;

import com.moncores.hackerhank.challenges.BaseSolutionTester;

/**
 *
 * @author Marlon Monçores - marlon@moncores.com
 */
public class SoltuonTester extends BaseSolutionTester {
    
    public SoltuonTester(){
        this.verbose = false;
        this.times = 10;
    }
}
