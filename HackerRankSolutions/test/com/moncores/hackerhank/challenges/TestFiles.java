package com.moncores.hackerhank.challenges;

import java.io.File;

/**
 *
 * @author marlo
 */
public class TestFiles {
    
    private File inputFile;
    private File expectedOutputFile;

    public File getInputFile() {
        return inputFile;
    }

    public void setInputFile(File inputFile) {
        this.inputFile = inputFile;
    }

    public File getExpectedOutputFile() {
        return expectedOutputFile;
    }

    public void setExpectedOutputFile(File expectedOutputFile) {
        this.expectedOutputFile = expectedOutputFile;
    }
}
