package com.moncores.hackerhank.challenges;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author marlo
 */
public abstract class BaseSolutionTester {
    protected boolean verbose = true;
    protected int times = 1;
    private final Map<Integer, TestFiles> loadedFiles = new HashMap<>();
    

    
    @Test
    public void runAllTestFiles() {
        runAll(this);
    }
    
    private static void runAll(BaseSolutionTester instance){
        String packageName = instance.getClass().getPackage().getName();
        String[] packageNames = packageName.split("\\.");
        String problemName = packageNames[packageNames.length-1];
        System.out.println(String.format("RUNNING TEST CASES FOR PROBLEM: %s",problemName));
        loadFiles(instance, problemName);
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;
        try{
            for (Map.Entry<Integer, TestFiles> entry : instance.loadedFiles.entrySet()) {
                for(int t=1;t<=instance.times;t++){
                    TestFiles tf = entry.getValue();

                    InputStream currentInput = new FileInputStream(tf.getInputFile());
                    System.setIn(currentInput);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    PrintStream ps = new PrintStream(baos);
                    System.setOut(ps);

                    Class solution = Class.forName(packageName+".Solution");
                    Method m = solution.getMethod("main", String[].class);
                    String[] params = null; // init params accordingly
                    long sT = System.currentTimeMillis();
                    m.invoke(null, (Object)params);
                    long timeElapsed = System.currentTimeMillis() - sT;

                    //now compare the algorithm out to the expected
                    System.out.flush();
                    List<String> readSolution = Arrays.asList(baos.toString().replace("\r\n", "\n").split("\n"));
                    List<String> expectedSolution = Files.readAllLines(tf.getExpectedOutputFile().toPath());

                    oldOut.println(String.format("TEST CASE %s - EXECUTION %s - TIME ELAPSED(ms): %s",entry.getKey(), t,timeElapsed));
                    if(instance.verbose){
                        oldOut.println("EXPECTED OUTPUT:");
                        printList(oldOut, expectedSolution);
                        oldOut.println("READ OUTPUT:");
                        printList(oldOut, readSolution);
                    }
                    Assert.assertEquals("SOLUTION IS DIFFERENT FROM EXPECTED!",expectedSolution, readSolution);
                }
                
            }
        } catch (IOException | ClassNotFoundException | IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException ex) {
            ex.printStackTrace(oldOut);
            throw new RuntimeException(ex);
        }catch (InvocationTargetException ite){
            ite.getCause().printStackTrace(oldOut);
            throw new RuntimeException(ite.getCause());
        } finally{
            System.setIn(oldIn);
            System.setOut(oldOut);
        }
    }
    
    private static void printList(PrintStream printTo, List<String> list){
        for (String elem : list) {
            printTo.println(elem);
        }
    }
    
    private static void loadFiles(BaseSolutionTester instance, String testName){
        File folder = new File("testCasesFiles/"+testName);
        File [] foundFiles = folder.listFiles(new FilenameFilter(){
            @Override        
            public boolean accept(File dir, String name) {
                String nameLC = name.toLowerCase();
                return nameLC.endsWith(".dat") && (nameLC.contains("-expected") || nameLC.contains("-input"));
            }
        });
                
        for (File currentFile : foundFiles){
            String[] nameS = currentFile.getName().split("-");
            if(nameS.length!=2) continue; //ignore current file
            Integer n = Integer.parseInt(nameS[0]);
            TestFiles loadedFile = instance.loadedFiles.get(n);
            if(loadedFile == null){
                loadedFile = new TestFiles();
                instance.loadedFiles.put(n, loadedFile);
            }
            if(nameS[1].contains("expected")){
                loadedFile.setExpectedOutputFile(currentFile);
            }else{
                loadedFile.setInputFile(currentFile);
            }
        }
        
        for (Map.Entry<Integer, TestFiles> entry : instance.loadedFiles.entrySet()) {
            TestFiles value = entry.getValue();
            String baseErrorMsg = "NO %s FILE FOR TEST CASE %s";
            if(value.getInputFile() == null){
                throw new RuntimeException(String.format(baseErrorMsg, "INPUT", entry.getKey()));
            }
            if(value.getExpectedOutputFile() == null){
                throw new RuntimeException(String.format(baseErrorMsg, entry.getKey()));
            }
        }
            
    }
}
