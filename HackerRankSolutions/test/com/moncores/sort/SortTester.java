/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moncores.sort;

import com.moncores.sort.BubbleSort;
import com.moncores.sort.HeapSort;
import com.moncores.sort.InsertionSort;
import java.util.Random;
import org.junit.Test;

/**
 *
 * @author marlo
 */
public class SortTester {
    
    @Test
    public void compareAlgorithms (){
        int size = 10000;
        int ratio = 1;
        int[] arr;
        long s;
        long t;
        
        arr = createRandomArray(size, 0, size/ratio);
        s = System.currentTimeMillis();
        InsertionSort.sort(arr);
        t = System.currentTimeMillis() - s;
        System.out.println(String.format("InsertionSort TAKES %s MILLISECONDS",t));
        
        
        arr = createRandomArray(size, 0, size/ratio);
        s = System.currentTimeMillis();
        BubbleSort.sort(arr);
        t = System.currentTimeMillis() - s;
//        System.out.println(Arrays.toString(arr));
        System.out.println(String.format("BubbleSort TAKES %s MILLISECONDS",t));
        
        arr = createRandomArray(size, 0, size/ratio);
        s = System.currentTimeMillis();
        HeapSort.sort(arr);
        t = System.currentTimeMillis() - s;
//        System.out.println(Arrays.toString(arr));
        System.out.println(String.format("HeapSort TAKES %s MILLISECONDS",t));
        
    }
    
    
    
    public static int[] createRandomArray(int size, int seed, int bound){
        int[] arr = new int[size];
        Random r = new Random(seed);
        for (int i = 0; i < size; i++) {
            arr[i] = r.nextInt(bound);
        }
        return arr;
    }
}
