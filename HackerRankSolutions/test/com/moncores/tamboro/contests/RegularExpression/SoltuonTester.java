package com.moncores.tamboro.contests.RegularExpression;

import com.moncores.hackerhank.challenges.BaseSolutionTester;

/**
 *
 * @author Marlon Monçores - marlon@moncores.com
 */
public class SoltuonTester extends BaseSolutionTester {
    
    public SoltuonTester(){
        this.verbose = false;
        this.times = 1;
    }
}
