//https://www.hackerrank.com/challenges/a-super-hero/problem
package com.moncores.hackerhank.challenges.ASuperHero;

import java.util.*;

/**
 *
 * @author Marlon Monçores
 */
public class Solution {
  
//    private static int totalR;
    private static int minCostGlobal;
    private static int calculateMinimumBullets(Map<Integer,Monster>[] levelsMonstersBullet){
        minCostGlobal = -1;
        int minimumBullets = calculateMinimumBulletsR(levelsMonstersBullet, 0, 0, 0);
        return minimumBullets;       
        
    }
    
    private static int calculateMinimumBulletsR (Map<Integer,Monster>[] levelsMonstersBullet, int accumulatedCost, int accumulatedBullets, int currentLevel){
        if(levelsMonstersBullet.length == currentLevel){//last level
            if(minCostGlobal == -1 || accumulatedCost < minCostGlobal){
                minCostGlobal = accumulatedCost;
            }
            return accumulatedCost;
        }
//        totalR++;
        Map<Integer,Monster> currentLevelMonsters = levelsMonstersBullet[currentLevel];
        int cheapestLevelElement = -1;
        for (Map.Entry<Integer, Monster> entry : currentLevelMonsters.entrySet()) {
            int power = entry.getKey();
            Monster m = entry.getValue();
            
            int monsterCost = accumulatedCost + (power > accumulatedBullets ? power - accumulatedBullets : 0); //current monster cost
            
            if(m.minCost != -1 && monsterCost >= m.minCost){//monster was processed and it's previous value is lower than current. Can abort monster current processing
                continue;//abort monster
            }
            if(minCostGlobal != -1 && minCostGlobal< monsterCost){//path over the global limit
                continue;
            }
            m.minCost = monsterCost;
            int totalCost = calculateMinimumBulletsR(levelsMonstersBullet, monsterCost, m.bullets, currentLevel+1);
            if(totalCost != -1  && (cheapestLevelElement == -1 || totalCost < cheapestLevelElement)){
                cheapestLevelElement = totalCost;
            }
        }
        return cheapestLevelElement;
    }
    
    
    private static class Monster{
        public int bullets=0;
        public int minCost=-1;
    }
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        /*
        1<= Tests<100
        1<= N <=100 (Levels)
        1<= M <= 5x10^5 (Monsters)
        1<= Power, Bullet <= 1000
        */
        Scanner in = new Scanner(System.in);
        String sTotalTests = in.nextLine();
        int totalTests = Integer.parseInt(sTotalTests);
        for (int currentTest=0; currentTest<totalTests; currentTest++){
            String[] levelEnemy = in.nextLine().split(" ");
            int totalLevel = Integer.parseInt(levelEnemy[0]);
            int totalEnemy = Integer.parseInt(levelEnemy[1]);
            int lastLevelWeakestMonster = -1;//best solution includes last level weakest monster, always.
            Map<Integer,Monster>[] levelsMonstersBullet = new Map[totalLevel];//max distinct monster power is 100
            int[][] monsterIndex = new int[totalLevel][totalEnemy];//list to save readMonsters realIndex
            
            for(int currentLevel=0;currentLevel<totalLevel;currentLevel++){
                String sLevel = in.nextLine();
                String[] levelM = sLevel.split(" ");
                for(int currentMonster=0;currentMonster<totalEnemy;currentMonster++){//save all monsters index
                    int power = Integer.parseInt(levelM[currentMonster]);
                    monsterIndex[currentLevel][currentMonster] = power;
                    if(currentLevel == totalLevel-1){
                        if(lastLevelWeakestMonster ==-1 || lastLevelWeakestMonster > power){
                            lastLevelWeakestMonster = power;
                        }
                    }
                }
            }
            
            for(int currentLevel=0;currentLevel<totalLevel;currentLevel++){
                Map<Integer,Monster> currentLevelMap = new HashMap<>();
                Map<Integer,Integer> currentLevelBulletMap = new HashMap<>(); //store for each bullet the weakest monster
                levelsMonstersBullet[currentLevel] = currentLevelMap;
                String sLevel = in.nextLine();
                String[] bulletsM = sLevel.split(" ");
                for(int currentMonster=0;currentMonster<totalEnemy;currentMonster++){
                    int power = monsterIndex[currentLevel][currentMonster];
                    int bullets = Integer.parseInt(bulletsM[currentMonster]);
                    if(currentLevel == totalLevel - 1) continue; //discard last level monsters!
                    
                    //check if current monster is part of the best solution
                    boolean canAdd=true;
                    for (Map.Entry<Integer, Monster> entry : currentLevelMap.entrySet()) {
                        Integer aPower = entry.getKey();
                        Integer aBullets = entry.getValue().bullets;
                        
                        if(aPower <= power && aBullets>=bullets){
                            canAdd = false;
                            break;//found a better monster
                        }
                    }
                    
                    if(canAdd){//added monster
                        Monster m = new Monster();
                        m.bullets = bullets;
                        currentLevelMap.put(power, m);
                        
                        
                        
                        //now check if any other monster can be removed
                        List<Integer> toBeRemovedList = new ArrayList<>();
                        for (Map.Entry<Integer, Monster> entry : currentLevelMap.entrySet()) {
                            Integer aPower = entry.getKey();
                            Integer aBullets = entry.getValue().bullets;
                            if(power < aPower && bullets>=aBullets){
                                toBeRemovedList.add(aPower);
                            }
                        }
                        
                        for (Integer toBeRemoved : toBeRemovedList) {
                            currentLevelMap.remove(toBeRemoved);
                        }
                            
                    }   
                }
                if(currentLevel == totalLevel - 1){//use only weakest monster in the last level. Bullets quantity don't matter.
                    Monster m = new Monster();
                    currentLevelMap.put(lastLevelWeakestMonster, m);
                }
            }
            
            //remove monsters that can't be on the optimal solution (has more power and give less bullets than any other
                    
            //end reading test input.
            Integer minimumBullets = calculateMinimumBullets(levelsMonstersBullet);
            System.out.println(minimumBullets);
//            System.err.println("RECURSIVE CALLS "+totalR);
            
        }
    }
}
