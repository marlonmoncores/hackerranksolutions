//https://www.hackerrank.com/challenges/ctci-array-left-rotation/problem
package com.moncores.hackerhank.crackingthecodinginterview.ArraysLeftRotation;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            int new_a = a_i - k;
            if(new_a <0){
                new_a += n;
            }
            a[new_a] = in.nextInt();
        }
        
        for(int a_i=0; a_i < n-1; a_i++){
            System.out.print(a[a_i] + " ");
        }
        System.out.print(a[n-1] + " ");
        
    }
}
