package com.moncores.hackerhank.tests.amazon.GreatestFlightRangeDrones;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Marlon Monçores - marlon@moncores.com
 */
public class Solution {
    
    /**
     * Select G Drones with the gratest flight range that are available;
     * 
     * @param numberOfRequiredDrones - Number of Drones that should be returned
     * @param drones - List containing all drones. Each drone has an Id and it's flight range
     * @param inMaintenanceDrones - Drones that are out due to maintence. They can't be returned
     * @return 
     */
    static List<Integer> greatestFlightRangeDrones(Integer numberOfRequiredDrones, List<Drone> drones, List<Integer> inMaintenanceDrones) {
        //sortDronesList
        NEXT_DRONE:
        for (int i=1;i< drones.size();i++){
            Drone x =drones.get(i);
            for(int r=0;r<inMaintenanceDrones.size();r++){
                if(inMaintenanceDrones.get(r).intValue() == x.id){
                    drones.remove(i--);
                    inMaintenanceDrones.remove(r);
                    continue NEXT_DRONE;
                }
            }
            int j=i-1;
            for (;j >= 0 && drones.get(j).flightRange < x.flightRange;j--){
                drones.set(j+1,drones.get(j));
            }
            drones.set(j+1,x);
        }
        List<Integer> r = new ArrayList<>(numberOfRequiredDrones);
        for (int i = 0; i < numberOfRequiredDrones; i++) {
            r.add(drones.get(i).id);
        }
        return r;
    }
    
    static List<Integer> greatestFlightRangeDrones_original(Integer numberOfRequiredDrones, List<Drone> drones, List<Integer> inMaintenanceDrones) {
        List<Integer> selectedDrones = new ArrayList<>(numberOfRequiredDrones);
        for(int i=0;i<numberOfRequiredDrones;i++){
            Drone currentBest = null; 
            int currentBestIndex = -1;
            NEXT_DRONE:
            for(int cd=0; cd<drones.size();cd++){
                Drone currentDrone = drones.get(cd);
                for(int md=0; md<inMaintenanceDrones.size();md++ ){
                    int maintenceD = inMaintenanceDrones.get(md);
                    if(currentDrone.getId() == maintenceD){//remove drone from both lists
                        drones.remove(cd--);
                        inMaintenanceDrones.remove(md--);
                        continue NEXT_DRONE; //ignore current drone
                    }
                }
                if(currentBest == null || currentDrone.getFlightRange()>currentBest.getFlightRange()){
                    currentBest = currentDrone;
                    currentBestIndex = cd;
                }
            }
            selectedDrones.add(currentBest.getId());
            drones.remove(currentBestIndex);
        }
        return selectedDrones;
    }
  
    private static class Drone{
        private Integer id;
        private Integer flightRange;
        
        public Drone(Integer id, Integer flightRange){
            this.id = id;
            this.flightRange = flightRange;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getFlightRange() {
            return flightRange;
        }

        public void setFlightRange(Integer flightRange) {
            this.flightRange = flightRange;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int numberOfDrones = in.nextInt();
        int numberOfRequiredDrones = in.nextInt();
        int numberOfDronesInMaintence = in.nextInt();
        List<Drone> drones = new ArrayList<>(numberOfDrones);
        List<Integer> inMaintenceDrones = new ArrayList<>(numberOfDronesInMaintence);
        
        for(int i=0;i< numberOfDrones;i++){
            drones.add(new Drone(in.nextInt(), in.nextInt()));
        }
        
        for(int i=0;i< numberOfDronesInMaintence;i++){
            inMaintenceDrones.add(in.nextInt());
        }
        
        List<Integer> greatestFlightRangeDrones = greatestFlightRangeDrones(numberOfRequiredDrones, drones, inMaintenceDrones);
        
        for (int i = 0; i < greatestFlightRangeDrones.size(); i++) {
            System.out.println(greatestFlightRangeDrones.get(i));
        }
    }
}
