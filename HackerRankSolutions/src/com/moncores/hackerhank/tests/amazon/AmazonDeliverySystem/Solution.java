package com.moncores.hackerhank.tests.amazon.AmazonDeliverySystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Marlon Monçores - marlon@moncores.com
 */
public class Solution {
    
    /**
     * Design a system that will calculate the minimum number of trips required to deliver a given list os packages
     * Constraints:
     *  * The maximum weight ("W") that can be delivered in a single trip
     *  * The weight of each package
     * This new system must follow these 3 rules:
     *  1. A single package cannot be split across multiple trips
     *  2. No more than 2(two) packages can be delivered per trip
     *  3. The heaviest package weights less or equal than "W"
     * 
     * You must return the minimum number of trips required to deliver all packages.
     * 
     * Sample Input:
     * W = 100
     * Packages' weight: 70, 10, 20
     * Expected Output: 2
     * 
     * @return 
     */
    static int minimumNumberOfTrips(int tripMaxWeight, int[] packagesWeight) {
        int [] solution = new int[packagesWeight.length];
        for(int i=0;i<solution.length;i++){
            solution[i] = i/2;//two packages per 
        }
        
        return 0;
    }
    
    static int original_minimumNumberOfTrips(int tripMaxWeight, int[] packagesWeight) {
        //Np-Hard problem. ->using best fit approach (may not give the right answer)
        int totalT = 0;
        for (int i=0;i<packagesWeight.length;i++){
            if(packagesWeight[i] ==-1)continue;//already choosed
            if(packagesWeight[i] == tripMaxWeight){//perfect fit
                totalT++;
                packagesWeight[i] = -1;
                continue;
            }
            
            int otherPackageIndex = -1;
            int currentLowerLoss = -1;
            
            for (int j=i+1;j<packagesWeight.length;j++){
                if(packagesWeight[j] == -1) continue; //already choosed
                int currentW = packagesWeight[i]+packagesWeight[j];
                if(currentW>tripMaxWeight) continue;//packages cant fit together  
                int currentLoss = tripMaxWeight - currentW;                   
                if(currentLowerLoss == -1 || currentLowerLoss> currentLoss){
                    currentLowerLoss = currentLoss;
                    otherPackageIndex = j;
                    if(currentLowerLoss ==0){//perfect fit
                        break;
                    }
                }
            }
            if(otherPackageIndex ==-1){//package will travel alone
                totalT++;
                packagesWeight[i] = -1;
            }else{
                totalT++;
                packagesWeight[i] = -1;
                packagesWeight[otherPackageIndex] = -1;
            }
        }
        return totalT;
        /*
        long sumWeight = 0l;
        double totalWeightLoss = 0l;
        for (int i=0;i<packagesWeight.length;i++){
            sumWeight+=packagesWeight[i]; //sum all weights - lower bound
            if(packagesWeight[i] == -1) continue;//already choosen
            
            int otherPackageIndex = -1;
            Integer currentLowerLoss = tripMaxWeight - packagesWeight[i];//if carry only one package
            
            for (int j=i;j<packagesWeight.length;j++){
                if(packagesWeight[j] == -1) continue;//already choosen
                int currentW = packagesWeight[i]+packagesWeight[j];
                if(currentW>tripMaxWeight) continue;//packages cant fit together                
                int currentLoss = tripMaxWeight - currentW;
                if(currentLowerLoss == null || (currentLoss<currentLowerLoss)){
                    currentLowerLoss = currentLoss;
                    otherPackageIndex = j;
                }
            }
            
            packagesWeight[i] = -1;
            if(otherPackageIndex>0){
                packagesWeight[otherPackageIndex] = -1;
            }
            totalWeightLoss += currentLowerLoss;
        }
        int minimalT = (int)(sumWeight+totalWeightLoss) / tripMaxWeight;
        if(tripMaxWeight % (sumWeight+totalWeightLoss) != 0){
            minimalT++;
        }
        
        return minimalT;*/
    }
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        
        int tripMaxWeight = in.nextInt();
        int totalPackages = in.nextInt();
        int[] packagesWeight = new int[totalPackages];
        
        for(int i=0;i<totalPackages;i++){
            packagesWeight[i] = in.nextInt();
        }
        
        int minimumNumberOfTrips = original_minimumNumberOfTrips(tripMaxWeight, packagesWeight);
        System.out.println(minimumNumberOfTrips);
    }
}
