package com.moncores.hackerhank.tests.amazon.AmazonDeliverySystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Marlon Monçores - marlon@moncores.com
 */
public class Devel {
    
    /**
     * Design a system that will calculate the minimum number of trips required to deliver a given list os packages
     * Constraints:
     *  * The maximum weight ("W") that can be delivered in a single trip
     *  * The weight of each package
     * This new system must follow these 3 rules:
     *  1. A single package cannot be split across multiple trips
     *  2. No more than 2(two) packages can be delivered per trip
     *  3. The heaviest package weights less or equal than "W"
     * 
     * You must return the minimum number of trips required to deliver all packages.
     * 
     * Sample Input:
     * W = 100
     * Packages' weight: 70, 10, 20
     * Expected Output: 2
     * 
     * @return 
     */
    static int minimumNumberOfTrips(int tripMaxWeight, int[] packagesWeight) {
        int [] solution = new int[packagesWeight.length];
        int best=-1;
        for(int i=0;i<solution.length;i++){
            solution[i] = 0;
        }//initial solution
        best = changeSolution(tripMaxWeight, packagesWeight, solution, 0);
        return best;
    }
    
    private static int changeSolution(int tripMaxWeight, int[] packagesWeight, int[] solution, int level){
        if(level == solution.length){
            int solutionCost = checkSolution(tripMaxWeight, packagesWeight, solution);
//            System.out.println(Arrays.toString(solution)+ " => COST:"+solutionCost);
            return solutionCost;
        }
        
        int best = -1;
        for(int j=1;j<=solution.length;j++){
            int solutionCost = changeSolution(tripMaxWeight, packagesWeight, solution, level+1);
            if(solutionCost > 0 &&(best<0 || best> solutionCost)){
                best=solutionCost;
            }
            solution[level] =j%solution.length;
        }
        return best;
    }
    
    private static int checkSolution (int tripMaxWeight, int[] packagesWeight, int[] solution){
        Map<Integer, int[]> packagesAndWeights = new HashMap<>();
        for(int i=0;i<packagesWeight.length;i++){
            Integer currentPackage = solution[i];
            Integer currentWeight = packagesWeight[i];
            int[] totalAndSumW = packagesAndWeights.get(currentPackage);
            if(totalAndSumW == null){
                totalAndSumW = new int[] {1,currentWeight};
                packagesAndWeights.put(currentPackage, totalAndSumW);
            }
            else {
                totalAndSumW[0] ++;
                totalAndSumW[1] += currentWeight;
            }
            
            if (totalAndSumW[0] > 2 || totalAndSumW[1] > tripMaxWeight)
                return -1;       
        }
        return packagesAndWeights.size();
    }
    
    public static void main(String[] args) {
        
        System.out.println(test0());
        System.out.println(test1());
        System.out.println(test2());
        System.out.println(test3());
        System.out.println(test4());
    }
    
    
    private static int test0(){
        int tripMaxWeight = 100;
        int[] packagesWeight = {70, 10,20}; //2
        return minimumNumberOfTrips(tripMaxWeight, packagesWeight);
    }
    
    private static int test1(){
        int tripMaxWeight = 10;
        int[] packagesWeight = {7, 1,2, 10}; //3
        return minimumNumberOfTrips(tripMaxWeight, packagesWeight);
    }
    
    private static int test2(){
        int tripMaxWeight = 10;
        int[] packagesWeight = {5, 8, 10, 1, 2 }; //3
        return minimumNumberOfTrips(tripMaxWeight, packagesWeight);
    }
    
    private static int test3(){
        int tripMaxWeight = 10;
        int[] packagesWeight = {10,10,10,3,9,8 }; //6
        return minimumNumberOfTrips(tripMaxWeight, packagesWeight);
    }
    
    private static int test4(){
        int tripMaxWeight = 100;
        int[] packagesWeight = {10,20,30,40,50,60,70,80,90,100}; //6
        return minimumNumberOfTrips(tripMaxWeight, packagesWeight);
    }
}
