/*
# Using any language of your choice, propose a function that matches a pattern
# and an input. The pattern is composed of alphanumeric characters (meaning
# simple comparison with the input), the ‘.’ means that any character in the
# input is allowed at the position, and ‘*’ means any repetition of the previous
# character is allowed, including none. The following test cases should pass:

assert match('a', 'a') == True
assert match('a', 'a*') == True
assert match('', 'a*') == True 
assert match('', 'a*a') == False
assert match('a', '.') == True
assert match('a', '.*') == True
assert match('ab', 'a*b*') == True
assert match('ab', 'a*.*b') == True
assert match('ab', 'a*c') == False
*/
package com.moncores.tamboro.contests.RegularExpression;

import java.util.*;

/**
 *
 * @author Marlon Monçores
 */
public class Solution {

    private static class Token{
        char token;
        boolean zeroOrMore = false;

        public Token(char token) {
            this.token = token;
        }
    }
    
    private static List<Token> tokenizeRegex(String regex){
        List<Token> tokens= new ArrayList<>();
        Token lastToken = null;
        for (int i=0;i<regex.length();i++){
            char current = regex.charAt(i);
            if(current != '*'){
                lastToken=new Token(current);
                tokens.add(lastToken);
            }else{
                lastToken.zeroOrMore = true;
            }
        }
        return tokens;
    }
    
    private static boolean match(String text, List<Token> regexTokens, int valueOffset, int tokenOffset){
        Character currentChar = (text.length()> valueOffset) ? text.charAt(valueOffset) : null;
        Token token = (regexTokens.size() > tokenOffset) ? regexTokens.get(tokenOffset) : null;
        
        
        //condições de parada
        if(token == null){
            return currentChar == null;//ambos acabaram = true || acabou a regex e ainda tem texto = false
        }
        if(!token.zeroOrMore && token.token!='.' && (currentChar== null || token.token != currentChar))
            return false;//texto não pode ser utilizado com o token
        
        boolean match = false;
        if(token.zeroOrMore){
            match = match(text, regexTokens,valueOffset, tokenOffset+1);//testa todas as opções do 0 (não usa o elemento atual)
            for(int i=valueOffset+1;i<=text.length() && match==false;i++){
                Character lastUsedChar = (text.length()> i-1) ? text.charAt(i-1) : null;
                if(token.token=='.' || lastUsedChar==null || token.token == lastUsedChar){
                    match = match(text, regexTokens,i, tokenOffset+1);//testa todas as opções a partir do 1 desde que seja '.' ou o caracter desejado
                }else{
                    break;//se nao casar com o token nao pode continuar
                }
            }
        }else{
            match = match || match(text, regexTokens,valueOffset+1, tokenOffset+1);//consome o token e consome o texto
        }
        return match;
    }
    
    
    public static Boolean match(String text, String regex){
        List<Token> tokens = tokenizeRegex(regex);
        return match(text, tokens, 0, 0);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String sTotalTests = in.nextLine();
        int totalTests = Integer.parseInt(sTotalTests);
        for (int currentTest=0; currentTest<totalTests; currentTest++){
            String[] value_regex = in.nextLine().split(" ");
            
            Boolean result = match(value_regex[0].replace("'",""), value_regex[1].replace("'",""));

            System.out.println(result);
        }
    }
}
