/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moncores.sort;

/**
 *
 * @author marlo
 */
public class BubbleSort {
    
    
    public static void sort (int[] arr){
        int n = arr.length;
        do{
            int newn = 0;
            for (int i = 1; i<n; i++){
                if (arr[i-1] > arr[i]){
                    int x=arr[i-1];
                    arr[i-1] = arr[i];
                    arr[i] = x;
                    newn = i;
                }
            }
            n = newn;
        }while(n > 0);
    }
}
