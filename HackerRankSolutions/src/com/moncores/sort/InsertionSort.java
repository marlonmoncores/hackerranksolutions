/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moncores.sort;

/**
 *
 * @author marlo
 */
public class InsertionSort {
    
    
    public static void sort (int[] arr){
        for (int i=1;i< arr.length;i++){
            int x =arr[i];
            int j=i-1;
            for (;j >= 0 && arr[j] > x;j--){
                arr[j+1] = arr[j];
            }
            arr[j+1] = x;
        }
    }
}
