/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moncores.sort;

/**
 *
 * @author marlo
 */
public class HeapSort {
    
    
    public static void sort (int[] arr){
        buildMaxHeap(arr);
        int n = arr.length;

        for (int i = arr.length - 1; i > 0; i--) {
            swap(arr, i, 0);
            maxHeapify(arr, 0, --n);
        }
    }
    
    
    private static void buildMaxHeap(int[] arr) {
        for (int i = arr.length / 2 - 1; i >= 0; i--)
            maxHeapify(arr, i, arr.length);

    }
    
    private static void maxHeapify(int[] arr, int pos, int size)  
    {  

         int max = 2 * pos + 1, right = max + 1;  
         if (max < size)  
         {  

             if (right < size && arr[max] < arr[right])  
                 max = right;

             if (arr[max] > arr[pos])  
             {  
                 swap(arr, max, pos);  
                 maxHeapify(arr, max, size);  
             }  
         }  
    }
    
    public static void swap(int[] v, int j, int aposJ) {
        int aux = v[j];
        v[j] = v[aposJ];
        v[aposJ] = aux;
    }
}
